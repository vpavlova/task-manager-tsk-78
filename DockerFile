FROM java:8

ADD ./target/task-manager.war /opt/task-manager/

EXPOSE 8080
ENTRYPOINT exec /usr/bin/java -jar /opt/task-manager/task-manager.war