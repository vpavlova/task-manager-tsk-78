<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<body>
<div class="container">
    <form class="form-signin" method="post" action="/auth">
        <sec:csrfInput/>
        <h2 class="form-signin-heading">Authorization</h2>
        <c:if test="${param.error ne null}">
            <div class="alert alert-danger" role="alert">User not found!</div>
        </c:if>
        <p>
            <label for="username" class="sr-only">Username</label>
            <input type="text" id="username" name="username" class="form-control" placeholder="Username" required=""
                   autofocus="">
        </p>
        <p>
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password"
                   required="">
        </p>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
    </form>
</div>