package ru.tsc.vpavlova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

    public AbstractUserOwnedModel(@NotNull final String name) {
        super(name);
    }

}