package ru.tsc.vpavlova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.vpavlova.tm.model.User;

import java.util.Optional;


public interface IUserRepository extends JpaRepository<User, String> {

    Optional<User> findFirstByLogin(@NotNull String login);

}