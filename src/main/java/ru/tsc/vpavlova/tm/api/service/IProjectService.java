package ru.tsc.vpavlova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.vpavlova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Transactional
    Project add(@NotNull String name, @Nullable final String userId);

    @Transactional
    Project add(@NotNull Project project, @Nullable final String userId);

    @Transactional
    void removeById(@NotNull String id, @Nullable final String userId);

    @Transactional
    @NotNull List<Project> findAll();

    @Transactional
    @Nullable Project findById(@NotNull String id);

    @Transactional
    Project save(@NotNull Project project, @Nullable final String userId);

    @Transactional
    void remove(@NotNull Project project, @Nullable final String userId);

    @Transactional
    void remove(@NotNull List<Project> projects);

    @Transactional
    boolean existsById(@NotNull String id);

    @Transactional
    void clear();

    @Transactional
    long count();
}
