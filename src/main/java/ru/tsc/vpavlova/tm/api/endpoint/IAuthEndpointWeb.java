package ru.tsc.vpavlova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.vpavlova.tm.model.CheckAuthResult;
import ru.tsc.vpavlova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpointWeb {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    CheckAuthResult login(@NotNull @WebParam(name = "username") String username,
                          @NotNull @WebParam(name = "password") String password
    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    User profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    CheckAuthResult logout();

}