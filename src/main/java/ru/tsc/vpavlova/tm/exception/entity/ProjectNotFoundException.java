package ru.tsc.vpavlova.tm.exception.entity;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}