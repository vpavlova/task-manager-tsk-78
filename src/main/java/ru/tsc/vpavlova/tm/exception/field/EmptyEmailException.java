package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}