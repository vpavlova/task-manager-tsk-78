package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}