package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User id is empty...");
    }

}