package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}