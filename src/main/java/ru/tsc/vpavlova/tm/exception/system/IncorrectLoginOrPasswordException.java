package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class IncorrectLoginOrPasswordException extends AbstractException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}