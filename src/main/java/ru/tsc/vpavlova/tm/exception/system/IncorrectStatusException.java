package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String value) {
        super("Error! Incorrect status. Value `" + value + "` not found...");
    }

}