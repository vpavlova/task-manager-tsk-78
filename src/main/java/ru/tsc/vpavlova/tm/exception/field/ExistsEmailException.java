package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}