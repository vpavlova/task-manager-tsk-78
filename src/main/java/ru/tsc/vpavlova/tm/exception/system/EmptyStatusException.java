package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}