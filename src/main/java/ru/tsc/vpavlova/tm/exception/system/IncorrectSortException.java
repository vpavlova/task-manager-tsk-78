package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class IncorrectSortException extends AbstractException {

    public IncorrectSortException(final String value) {
        super("Error! Incorrect sort. Value `" + value + "` not found...");
    }

}