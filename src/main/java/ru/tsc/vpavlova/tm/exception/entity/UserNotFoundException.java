package ru.tsc.vpavlova.tm.exception.entity;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}