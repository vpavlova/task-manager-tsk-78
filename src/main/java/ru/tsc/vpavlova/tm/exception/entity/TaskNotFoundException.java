package ru.tsc.vpavlova.tm.exception.entity;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}