package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}