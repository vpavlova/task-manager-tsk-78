package ru.tsc.vpavlova.tm.exception.field;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}