package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not supported...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Command `" + command + "` not supported...");
    }

}