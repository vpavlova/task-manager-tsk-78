package ru.tsc.vpavlova.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}