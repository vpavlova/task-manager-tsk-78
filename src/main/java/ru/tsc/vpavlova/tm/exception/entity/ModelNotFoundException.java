package ru.tsc.vpavlova.tm.exception.entity;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}