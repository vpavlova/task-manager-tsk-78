package ru.tsc.vpavlova.tm.exception.system;

import ru.tsc.vpavlova.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}